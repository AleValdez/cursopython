#num1 = int(input("Primer número:"))
#num2 = int(input("Segundo número:"))
#num3 = int(input("Tercero número:"))

def comparar_menor(valor1, valor2, valor3):
    return valor1 < valor2 < valor3

def comparar_mayor(valor1, valor2, valor3):
    return valor1 > valor2 > valor3

#print(comparar_menor(num1, num2, num3))


class PasswordValidador:
    def __init__(self, password):
        self.__password = password
    
    def cantidad_caracters(self, password, num) -> bool:
        pass

    def contiene_numero(self, password) -> bool:
        pass

    def contiene_caracter_especial(self, password) -> bool:
        pass

    def esValido(self, password) -> bool:
        return self.contiene_caracter_especial(password) and self.contiene_numero(password) and self.cantidad_caracters(password,8)


#nueva_lista = [expresion for variable in iterable if condicion]

pares = [ num * 2 for num in range(10)]

numeros = [num for num in range(30)]

impares = [ num for num in numeros if num % 2 != 0] 

#print(pares, numeros, impares)

from datetime import datetime

def medir_tiempo(func):
    def wrapper(*args, **kwargs):
        inicio = datetime.now()
        resultado = func(*args, **kwargs)
        fin = datetime.now()
        tiempo = fin - inicio
        print(f"Tiempo de ejecución: {tiempo}")
        return resultado
    return wrapper

@medir_tiempo
def imprimir_mensaje(texto):
    print(texto)

#imprimir_mensaje("Hola, tomando el tiempo de la función")

def log_medir_tiempo(nombre_archivo):
    def medir_tiempo(func):
        def wrapper(*args, **kwargs):
            inicio = datetime.now()
            resultado = func(*args, **kwargs)
            fin = datetime.now()
            tiempo = fin - inicio
            with open(nombre_archivo, 'a') as archivo_txt:
                archivo_txt.write(f"{func.__name__} -> tiempo de ejecución de la funcion {tiempo} \n")
            return resultado
        return wrapper
    return medir_tiempo

@log_medir_tiempo("logs.txt")
def imprimir_mensaje(texto):
    print(texto)

@log_medir_tiempo("logs.txt")
def saludo():
    print("Holaaaaa")

#imprimir_mensaje("Hola, tomando el tiempo de la función")
#saludo()

def numeros_pares():
    for i in range(1,10):
        if i % 2 == 0:
            yield i

for num in numeros_pares():
    print(num)

generador = numeros_pares()

try:
    print(next(generador))
    print(next(generador))
    print(next(generador))

    print(next(generador))
    print(next(generador))
except StopIteration:
    print("No puede iterar más")

class Contador:
    def __init__(self, fin , inicio=0):
        self.__fin = fin
        self.__inicio = inicio

    def __iter__(self):
        return self
    
    def __next__(self):
        if self.__inicio >= self.__fin:
            raise StopIteration
        
        resultado = self.__inicio + 1
        self.__inicio += 1
        return resultado
    
    def __str__(self):
        return "Soy un contador"

generador_clase = Contador(18)

for elem in generador_clase:
    print(elem)

#https://kinsta.com/es/blog/generar-claves-ssh/

print("Nuevo cambio")